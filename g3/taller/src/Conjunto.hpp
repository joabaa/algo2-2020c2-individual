
template <class T>
Conjunto<T>::Conjunto() : _raiz(NULL), _cantidad(0){
    // Completar
}

template <class T>
Conjunto<T>::~Conjunto() { 
    // Completar
    destruir(_raiz);
}
template <class T>
void Conjunto<T>::destruir(Nodo* n){
    if(n != NULL){
       destruir(n->der);
       destruir(n->izq);
       delete n;
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    if(_cantidad == 0)//Podria revisar si la raiz es NULL, pero basta con ver que el arbol esta vacío.
        return false;
    Nodo* actual = this->_raiz;
    while(actual != NULL){
        if(actual->valor == clave){
            return true;
        }else{
            if(clave < actual->valor){
                actual = actual->izq;
            }else{
                actual = actual->der;
            }
        }
    }
    return false;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if (_cantidad == 0) {//Con la estructura de Pertenece puedo implementar la inserción.
        Nodo* nuevoNodo = new Nodo(clave);
        _raiz = nuevoNodo;
        _cantidad++;
    }else {
        Nodo* actual = this->_raiz;

        bool claveEncontrada = false;
        while (actual != NULL && !claveEncontrada) {
            if(actual->valor != clave) {
                if (clave < actual->valor) {
                    if(actual->izq == NULL){
                    Nodo* nuevoNodo = new Nodo(clave);
                    actual->izq = nuevoNodo;
                    nuevoNodo->_padre = actual;
                    _cantidad++;
                    }else{
                        actual = actual->izq;
                    }

                } else {
                    if(actual->der == NULL){
                    Nodo* nuevoNodo = new Nodo(clave);
                    actual->der = nuevoNodo;
                    nuevoNodo->_padre = actual;
                    _cantidad++;
                    }else{
                        actual = actual->der;
                    }

                }
            }else{
                claveEncontrada = true;
            }
        }
    }

}

template <class T>
void Conjunto<T>::remover(const T& clave){
    if(_cantidad != 0){
        Nodo* temp = _raiz;
        bool found = false;
        bool fin = false;
        while(not(fin) and not(found)){
            if(temp->valor == clave){
                found = true;
            }else{
                if(temp->valor < clave){
                    if(temp->der == NULL){
                        fin = true;
                    }else{
                        temp = temp->der;
                    }
                }else{
                    if(temp->izq == NULL){
                        fin = true;
                    }else{
                        temp = temp->izq;
                    }
                }
            }
        }
        if(found){
            if(temp->der == NULL){
                if(temp->izq == NULL){//Caso en que el nodo a remover no tiene hijos.
                    if(temp->_padre == NULL){//El nodo es raiz.
                        _raiz = NULL;//Es necesario?, o al eliminar temp esto automaticamente apunta a NULL?
                        delete temp;
                        _cantidad--;
                    }else{//El nodo no es raiz.
                        if(temp->_padre->izq == NULL){//El padre del nodo no tiene hijo izquierdo, entonces el nodo es hijo derecho.
                            temp->_padre->der = NULL;
                            delete temp;
                            _cantidad--;
                        }else{//El padre del nodo tiene hijo izquierdo.
                            if(temp->_padre->izq->valor == temp->valor){//El nodo es hijo izquierdo.
                                temp->_padre->izq = NULL;
                                delete temp;
                                _cantidad--;
                            }else{//El nodo es hijo derecho.
                                temp->_padre->der = NULL;
                                delete temp;
                                _cantidad--;
                            }
                        }
                    }
                }else{//Caso en que el nodo a remover tiene solo hijo izquierdo.
                    if(temp->_padre == NULL){//El nodo es raiz.
                        _raiz = temp->izq;
                        temp->izq->_padre = NULL;
                        delete temp;
                        _cantidad--;
                    }else{//El nodo no es raiz.
                        if(temp->_padre->izq == NULL){//El padre no tiene hijo izq, entonces es hijo der.
                            temp->_padre->der = temp->izq;
                            temp->izq->_padre = temp->_padre;
                            delete temp;
                            _cantidad--;
                        }else{//El padre tiene hijo izq.
                            if(temp->_padre->izq->valor == temp->valor){//El nodo es hijo izq.
                                temp->_padre->izq = temp->izq;
                                temp->izq->_padre = temp->_padre;
                                delete temp;
                                _cantidad--;
                            }else{//El nodo es hijo der.
                                temp->_padre->der = temp->izq;
                                temp->izq->_padre = temp->_padre;
                                delete temp;
                                _cantidad--;
                            }
                        }
                    }
                }
            }else{
                if(temp->izq == NULL){//Caso en que el nodo a remover tiene solo hijo derecho.
                    if(temp->_padre == NULL){//EL nodo es raiz
                        _raiz = temp->der;
                        temp->der->_padre = NULL;
                        delete temp;
                        _cantidad--;
                    }else{//El nodo no es raiz.
                        if(temp->_padre->izq == NULL){//El padre no tiene hijo izq, entonces es hijo der.
                            temp->_padre->der = temp->der;
                            temp->der->_padre = temp->_padre;
                            delete temp;
                            _cantidad--;
                        }else{//El padre tiene hijo izq.
                            if(temp->_padre->izq->valor == temp->valor){//El nodo es hijo izq.
                                temp->_padre->izq = temp->der;
                                temp->der->_padre = temp->_padre;
                                delete temp;
                                _cantidad--;
                            }else{//El nodo es hijo derecho.
                                temp->_padre->der = temp->der;
                                temp->der->_padre = temp->_padre;
                                delete temp;
                                _cantidad--;
                            }
                        }
                    }
                }else{//El nodo tiene hijo izq y der.
                    Nodo* min = temp->der;
                    while(min->izq != NULL){//Busco el minimo del arbol derecho del nodo.
                        min = min->izq;
                    }
                    if(temp->_padre == NULL){//El nodo es raiz.
                        temp->der->_padre = NULL;
                        _raiz = temp->der;//??????????????????????????????????????????????????
                        min->izq = temp->izq;
                        temp->izq->_padre = min;
                        delete temp;
                        _cantidad--;
                    }else{//El nodo no es raiz.
                        if(temp->_padre->izq == NULL){//El padre no tiene hijo izq, entonces es hijo der.
                            temp->_padre->der = temp->der;//?????????????????????????????????????????
                            temp->der->_padre = temp->_padre;
                            min->izq = temp->izq;
                            temp->izq->_padre = min;
                            delete temp;
                            _cantidad--;
                        }else{//El padre tiene hijo izq y der.
                            if(temp->_padre->izq->valor == temp->valor){//El nodo es hijo izq.
                                temp->_padre->izq = temp->der;
                                temp->der->_padre = temp->_padre;
                                min->izq = temp->izq;
                                temp->izq->_padre = min;
                                delete temp;
                                _cantidad--;
                            }else{//El nodo es hijo derecho.
                                temp->_padre->der = temp->der;
                                temp->der->_padre = temp->_padre;
                                min->izq = temp->izq;
                                temp->izq->_padre = min;
                                delete temp;
                                _cantidad--;
                            }
                        }
                    }
                }
            }
        }
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) { //se refiere al sucesor inmediato.
    Nodo* actual = _raiz;        //busco el elemento que se me dio por parametro.
    bool loEncontre = false;
    while(actual != NULL && loEncontre == false){
        if(actual->valor == clave){
            loEncontre = true;
        }else{
            if(actual->valor > clave){
                actual = actual->izq;
            }else{
                actual = actual->der;
            }
        }
    }
    if(actual->der != NULL){
        Nodo* minimo = actual->der;
        while(minimo->izq != NULL){
            minimo = minimo->izq;
        }
        return minimo->valor;
    }else{
        if(actual->valor < actual->_padre->valor){ //acá chequeo que mi nodo va a ser un hijo izquierdo del padre
            return actual->_padre->valor;
        }else{
            while(actual->_padre->valor < actual->valor){
                actual = actual->_padre;
            }
            return actual->valor;
        }
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* min = _raiz;

    while(min->izq != NULL){
        min = min->izq;
    }
    return min->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* max = _raiz;

    while(max->der != NULL){
        max = max->der;
    }
    return max->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {

    return this->_cantidad;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

