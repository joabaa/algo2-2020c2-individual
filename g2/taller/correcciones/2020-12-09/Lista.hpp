#include "Lista.h"

Lista::Lista() {
    // Completar
    head = nullptr;
    tail = nullptr;
    length_ = 0;
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    // Completar
    _destruir();
}

void Lista::sacarPrimero(){
    Nodo* p = head;
    head = head->sig;
    delete p;
}
void Lista::_destruir(){
    while(head !=nullptr){
        sacarPrimero();
    }
}


Lista& Lista::operator=(const Lista& aCopiar) {
    // Completar
    while(this->head != nullptr){
        eliminar(0);
    }
    int i = 0;
    while(this->length_ < aCopiar.longitud()){
        this->agregarAtras(aCopiar.iesimo (i));
        i++;
        }

    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    // Completar
    if(this->length_ == 0){
        Nodo *nd = new Nodo;
        nd->data = elem;
        nd->sig = this->head;
        nd->prev = nullptr;

        this->head = nd;
        this->tail = nd;
        this->length_++;

    }else {
        Nodo *nd = new Nodo;
        nd->data = elem;
        nd->sig = this->head;
        nd->prev = nullptr;

        this->head->prev = nd;

        this->head = nd;
        this->length_++;

        }

}

void Lista::agregarAtras(const int& elem) { //el razonamiento es "espejado" al de agregarAdelante
    // Completar
    if(this->length_ == 0){
        Nodo *nd = new Nodo;
        nd->data = elem;
        nd->sig = nullptr;
        nd->prev = this->tail;

        this->tail = nd;
        this->head = nd;
        this->length_++;

        
    }else {
        Nodo *nd = new Nodo;
        nd->data = elem;
        nd->sig = nullptr;
        nd->prev = this->tail;

        this->tail->sig =nd;

        this->tail = nd;
        this->length_++;


    }

}

void Lista::eliminar(Nat i) {
    // Completar
    Nodo* borrado;
    if(this->length_ == 1){ //Si la lista tiene 1 solo elemento, la lista vuelve a ser la inicializada
        borrado = head;
        head = nullptr;
        tail = nullptr;
        delete(borrado);
        length_ --;
    }else if(i==0){                     //si borramos el primer nodo
        borrado = head;

        head = borrado->sig;
        head->prev = nullptr;
        delete(borrado);

        length_ --;

    }else if(i ==this->length_ - 1){ //Si estoy borrando la última posicion
        borrado = tail;

        tail = borrado->prev;
        tail->sig = nullptr;
        delete(borrado);

        length_ --;

    }else{                         // Si el iesimo esta entre el primero y el último
        borrado = head;

        int iesimo = 0;
        while(iesimo < i){
            borrado = borrado->sig;
            iesimo ++;
        }
        borrado->sig->prev = borrado->prev;
        borrado->prev->sig =borrado->sig;
        delete(borrado);

        length_ --;

    }

}

int Lista::longitud() const {
    // Completar
    return this->length_;
//    int contador=0;
//    Nodo* aux = header->sig;
//    while(aux != trailer){
//        contador++;
//        aux= aux->sig;
//    }
//    return contador;
}

const int& Lista::iesimo(Nat i) const {
    // Completar
    Nodo* aux;
    if(i>=0 && i < this->longitud()){
        aux = this->head;
        for(int j=0; j<i;j++){
            aux = aux->sig;
        }
        return aux->data;
    }
}


int& Lista::iesimo(Nat i) {
    // Completar (hint: es igual a la anterior...)
 Nodo* aux;
 int valor = 0;
 if(i>=0 && i < this->longitud()){
     aux = this->head;
     for(int j=0; j<i;j++){
         aux = aux->sig;
     }

     return aux->data;
 }

}

void Lista::mostrar(ostream& o) {
    // Completar
}
