#include <iostream>
#include <stdio.h>
#include <wchar.h>
#include <locale.h>
#include <vector>
#include <stdlib.h>
#include <list>
using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
    Fecha(int mes, int dia);
    uint mes();
    uint dia();
    void incrementar_dia();


    bool operator==(Fecha o);


  private:
    //Completar miembros internos9
    uint dia_;
    uint mes_;
};
Fecha::Fecha(int mes, int dia) : mes_ (mes), dia_ (dia)  {
    if(mes >= 1&& mes <=12){
        uint diaMax = dias_en_mes(mes);
        if(dia >=1 && dia<= diaMax){
            this->mes_ = mes;
            this->dia_ = dia;
        }
    }else{
        this->mes_ =12;                         //ESTO LO PONEMOS ASI X CONVENCION
        uint diaMax = dias_en_mes(12);     //
        this->dia_ = diaMax;
    }
}

uint Fecha::mes() {
    return this-> mes_;
}

uint Fecha::dia() {
    return this-> dia_;
}
//ej 10
void Fecha::incrementar_dia(){
    uint diaMax = dias_en_mes(this->mes_);
    if(diaMax == this->dia_){
        this->dia_ = 1;
        if(this->mes_ == 12){
            mes_ = 1;
        }else{
            mes_++;
        }

    }else{
        dia_++;
    }
}


//ejercicio 8
ostream& operator<<(ostream& os, Fecha f){
   return os << f.dia() << "/" << f.mes();
}


bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia() && this->mes() == o.mes();
    // Completar iguadad (ej 9)
    return igual_dia;
}


// Ejercicio 11, 12

// Clase Horario
class Horario{
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario h);
    bool operator<(Horario h);

private:
    uint hora_;
    uint min_;

};
Horario::Horario(uint hora, uint min): hora_(hora), min_(min) {   //si la hora no es válida, setea las 00:00 hs
    if(hora >= 00 && hora <24 && min>=00 && min<60){
        hora_ = hora;
        min_ = min;
    }else{
        hora_ = 00;
        min_ = 00;
    }
}

uint Horario::hora(){
    return this->hora_;
}
uint Horario::min(){
    return this->min_;
}

ostream& operator<<(ostream& os, Horario h) {                //sobreescribo la impresion
    os << h.hora() << ":" << h.min();
    return os;
}
bool Horario::operator==(Horario h) {
    bool igual_horario = this->hora() == h.hora() && this->min() == h.min();
    // Completar iguadad (ej 9)
    return igual_horario;
}
bool Horario::operator<(Horario h){
    bool esMenor;
    if(this->hora_ < h.hora()){
        esMenor = true;
    }else if (this->hora_ == h.hora()){
        if(this->min_ < h.min()){
            esMenor = true;
        }else{
            esMenor = false;
        }
    }else{
        esMenor = false;
    }
    return  esMenor;
}

// Ejercicio 13

// Clase Recordatorio
class Recordatorio{
public:
    Recordatorio(Fecha f,Horario h,string m);
    string mensaje();
    Fecha darFecha();
    Horario darHorario();

private:
    string m_;
    Fecha f_;
    Horario h_;

};
Recordatorio::Recordatorio(Fecha f, Horario h, string m) :f_(f),h_(h),m_(m) {}

string Recordatorio::mensaje(){
    return this->m_;
}
Fecha Recordatorio::darFecha() {
    return this->f_;
}
Horario Recordatorio::darHorario() {
    return this->h_;
}

ostream& operator<<(ostream& os, Recordatorio r) {                //sobreescribo la impresion
    os << r.mensaje() << " @ " << r.darFecha() << " " << r.darHorario();
    return os;
}
ostream& operator+(ostream& os, Recordatorio r) {                //sobreescribo la impresion
    os << r.mensaje() << " @ " << r.darFecha() << " " << r.darHorario();
    return os;
}
// Ejercicio 14

// Clase Agenda
class Agenda{
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list <Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    list <Recordatorio> rec_;
    Fecha fecha_;

};

Agenda::Agenda(Fecha f): fecha_(f), rec_(){
}

void Agenda::agregar_recordatorio(Recordatorio r) {
    this->rec_.push_back(r);
}
void Agenda::incrementar_dia() {
    this->fecha_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    Fecha diaActual = this->fecha_;
    list<Recordatorio> rec;
    for( Recordatorio r : rec_){
        if(diaActual == r.darFecha())
            rec.push_back(r);
    }
    return rec;
}

Fecha Agenda::hoy(){
    return this->fecha_;
}
string toString(list<Recordatorio> r){
    vector<Recordatorio> rvec;
    string mensaje;
    for(Recordatorio rec : r){
        rvec.push_back(rec);
    }
    if(rvec.size()!= 0) {
        Recordatorio aux = rvec[0];
        for (int i = 0; i < rvec.size() - 1; i++) {

            for (int j = i + 1; j < rvec.size(); j++) {
                if (rvec[j].darHorario() < rvec[i].darHorario()) {
                    aux = rvec[i];
                    rvec[i] = rvec[j];
                    rvec[j] = aux;
                }
            }

        }
    }
    for(int i = 0; i<rvec.size();i++){
        string dia = to_string(rvec[i].darFecha().dia());
        string mes = to_string(rvec[i].darFecha().mes());
        string hora =to_string(rvec[i].darHorario().hora());
        string minuto =(to_string(rvec[i].darHorario().min()));
     mensaje = mensaje + rvec[i].mensaje()+" @ " +dia+"/"+mes +" "+hora+":"+minuto +"\n";
    }
    return mensaje;
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() <<"\n"<< "=====" << "\n" << toString(a.recordatorios_de_hoy());
    return os;
}