template <class T>
string_map<T>::string_map(): raiz(nullptr),_size(0){
    // COMPLETAR
	raiz=new Nodo();
}

template <class T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <class T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    // COMPLETAR
    this->~string_map();
    this->_size=0;
	this->raiz=new Nodo();
	if(d.raiz->definicion){
		this->raiz->definicion=new T(*(d.raiz->definicion));
		this->raiz->path=new std::string("");
	}
    agregar(d.raiz);
	return *this;   
}

template <class T>
void string_map<T>::agregar(Nodo* n){
	for(Nodo* s: n->siguientes){
		if(s!=nullptr){
			if(s->definicion!=nullptr)
				this->insert(make_pair(*(s->path),*(s->definicion)));
			if(!hijosDe(s)==0)
				agregar(s);
		}
	}
}

template <class T>
string_map<T>::~string_map(){
    // COMPLETAR
    deletear(raiz);
    
}

template <class T>
void string_map<T>::deletear(Nodo* n){
	for(Nodo* s: n->siguientes){
		if(s!=NULL){
			deletear(s);
			}
		}
	if(n->definicion!=NULL){
		delete n->definicion;
		delete n->path;
	}
	delete n;
}
/*
template <class T>
bool string_map<T>::esHoja(Nodo *n)const{
	bool res=true;
	if(n==nullptr)
		return true;
	for(int i=0;i<n->siguientes.size();i++){
		res &= n->siguientes[i]==nullptr;
	}
	return res;
}
*/
template <class T>
void string_map<T>::insert(const std::pair<string, T>&d){
	if(d.first==""){
		raiz->definicion=new T(d.second);
		raiz->path=new std::string("");
	}
	Nodo* nodo=raiz;
    
    std::string clave=d.first;
    T def=d.second;
    for(int i=0;i<clave.size();i++){
    	if(nodo->siguientes[(int)clave[i]]==nullptr){
    		nodo->siguientes[(int)clave[i]]=new Nodo();
    	}
    	nodo=nodo->siguientes[(int)clave[i]];
    }
	if(nodo->definicion){
		delete nodo->definicion;
		delete nodo->path;
	}
    nodo->definicion=new T(def);
	nodo->path=new std::string(clave);
	_size++;
}

template <class T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
    if(count(clave)==0)
    	insert(std::make_pair(clave, T()));
    return at(clave);
    	
}


template <class T>
int string_map<T>::count(const string& clave) const{
    // COMPLETAR
    if(!raiz)
    	return 0;
    
    int res=0;
    Nodo* nodo=raiz;
    int i=0;
    while(nodo->siguientes[(int)clave[i]]!=nullptr){
    	nodo=nodo->siguientes[(int)clave[i]];
    	i++;
    }
    if(i==clave.size() && (nodo->definicion!=nullptr))
    	res=1;
    return res;
  
}

template <class T>
const T& string_map<T>::at(const string& clave) const {
    // COMPLETAR
    Nodo* nodo=raiz;
    int i=0;
    while(i<clave.size()){
    	nodo=nodo->siguientes[(int)clave[i]];
    	i++;
    }
    return *(nodo->definicion);
}

template <class T>
T& string_map<T>::at(const string& clave) {
    // COMPLETAR
    Nodo* nodo=raiz;
    int i=0;
    while(i<clave.size()){
    	nodo=nodo->siguientes[(int)clave[i]];
    	i++;
    }
    return *(nodo->definicion);
}

template <class T>
void string_map<T>::erase(const string& clave) {
    // COMPLETAR
    Nodo* nodo=raiz;
    Nodo* ultimo=raiz;
    
	int i=0;
	int ultimoIndice=0;
	while(i<clave.size()-1){
		if(hijosDe(nodo)>1||nodo->definicion){
			ultimo=nodo;
			ultimoIndice=i;
		}
		nodo=nodo->siguientes[(int)clave[i]];
		i++;
	}

	delete nodo->siguientes[(int)clave[i]]->definicion;
	nodo->siguientes[(int)clave[i]]->definicion=nullptr;
	delete  nodo->siguientes[(int)clave[i]]->path;
	nodo->siguientes[(int)clave[i]]->path=nullptr;
	if(hijosDe(nodo->siguientes[(int)clave[i]])>0)return;
	delete nodo->siguientes[(int)clave[i]];
	nodo->siguientes[(int)clave[i]]=nullptr;
	borrar(ultimoIndice,clave.size()-1,clave);
}

template<class T>
void string_map<T>::borrar(const int desde,int hasta,std::string clave){
	Nodo* n=raiz;
	int i=0;
	for(i=0;i<desde;i++){
		n=n->siguientes[(int)clave[i]];
	}
	if(desde==hasta)
		return;
	while(i<hasta-1){
		n=n->siguientes[(int)clave[i]];
		i++;
	}
	delete n->siguientes[(int)clave[i]];
	n->siguientes[(int)clave[i]]=nullptr;
	n=nullptr;
	borrar(desde,hasta-1,clave);
}

template<class T>
int string_map<T>::hijosDe(Nodo* n)const{
	int res=0;
	for(int i=0;i<n->siguientes.size();i++){
		if(n->siguientes[i]!=nullptr)
			res++;
	}
	return res;
}

template <class T>
int string_map<T>::size() const{
    // COMPLETAR
    return _size;
}

template <class T>
bool string_map<T>::empty() const{
    // COMPLETAR
    return (_size==0)?true:false;
}
